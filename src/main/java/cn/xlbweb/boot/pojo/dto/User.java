package cn.xlbweb.boot.pojo.dto;

import lombok.Data;

/**
 * @author: bobi
 * @date: 2020-05-17 10:31
 * @description:
 */
@Data
public class User {

    private Integer id;
    private String username;
    private String password;
}
