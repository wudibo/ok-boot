package cn.xlbweb.boot.curd;

import cn.xlbweb.boot.AppTests;
import cn.xlbweb.boot.pojo.dto.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author: bobi
 * @date: 2020-05-16 23:59
 * @description:
 */
public class RedisCrudTest extends AppTests {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test() {
        redisTemplate.opsForValue().set("abc", "123");
        Object hello = redisTemplate.opsForValue().get("hello");
        System.out.println(hello);
    }

    @Test
    public void test2() {
        User user = new User();
        user.setId(1);
        user.setUsername("admin");
        user.setPassword("123456");
        redisTemplate.opsForValue().set("user:1", user);
        Object object = redisTemplate.opsForValue().get("user:1");
        System.out.println(object);
    }

    @Test
    public void test3() {
    }
}
